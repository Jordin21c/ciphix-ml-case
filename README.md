# OBJECTIVE

**Problem Description**
```
AtCiphix,you arepart ofateamthatreceives 1000 sofquestionsfromclients
pertainingtoavarietyoftopicseveryday.Recently,oneofyourcolleagueshada
great idea. Why not use conversational technology to create a chatbot that

- is available 24 / 7 to serve your clients
- frees up time for your team so that they can focuson tasks that they enjoy and find truly valuable.

All in all, brilliant idea!

Now on to the challenging part. To understand a conversation with a user,
chatbots make use of natural language processing. This is referred to as “the
brain” of the robot. This is why, before the chatbot can understand a conversation,
we have to design and train its brain. To this end, one first needs to identify the
types of conversations that the chatbot should be able to have and the kind of
knowledge the chatbot should possess. Being highly skilled in data science and
machine learning, this is where you come in.

Attached to this case you can find _unstructured_ data.Your team has collected
this data to give you an overview of the questions that are received on a daily
basis. Your task is simple: make sense of this data and come up with a design for
the chatbot’s brain.

To be more explicit about this task: you are expected to find patterns in this
dataset and come up with 10 types of questions your colleague should focus on
first. Obviously, your means and decisions should be justified. Below you can find
requirements that should give you a better understanding of what is expected of
you.
```


```
Obtaining Data: Data can be downloaded from:https://ciphix.io/ai/data.csv
```
**Requirements:**
```
Important: Your laptop could have trouble with thesize of the data, think about
efficient code and smart methods of dealing with this challenge. It could save a
lot of time.
```
```
1. Use Python
2. Perform EDA on your data
3. Preprocess your data appropriately.
4. Use at least 2 machine learning algorithms in order to make sense of the
data.
5. Give another example(s) of ML algorithms and why these could be used for
this problem. (note: this means that you don’t haveto write code,
explanation suffices)
6. Choose the best method by evaluating the different algorithms. Present
these findings in a way such that we can easily understand what you did.
7. Provide us with the top 10 questions/types of conversations.
8. Your code should contain a notebook, which will guide us through your
project.
9. Bonus: deploy your best model and explain how we can use it.
```
**Evaluation of assignment**
```
We evaluate your assignment based on the following points:
```
```
1. Quality of your code (structure, readability, etc.)
2. Efficiency and scalability of your solution
3. Technical skills (machine learning algorithms, technical approach etc.)
4. Creativity on visualizing/presenting your results.
```
**Submission of assignment**

```
Hand-in: The top 10 clusters, your script(s) and yourfindings.
```
```
Either:
```
```
1. Zip your submission using the following naming format:
Ciphix_MLAssignment_LastName_ddmmyy.zip
2. WeTransfer your submission file to your technical buddy mentioned in the
mail.
```
```
Or:
```
```
1. Track your progress on Github
```
```
2. Share your repo link with your technical buddy mentioned in the mail
```
